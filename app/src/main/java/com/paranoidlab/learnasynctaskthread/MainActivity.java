package com.paranoidlab.learnasynctaskthread;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private EditText editText;
    private Button button;
    private String[] listOfImage;
    private ProgressBar progressBar;
    private LinearLayout loadingSectionb = null; //used when we were using theread
    private ListView listView;
    private ProgressBar asyncProgressBar;
    private Button asyncTaskButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        loadingSectionb = (LinearLayout) findViewById(R.id.linearLayout);
        listOfImage = getResources().getStringArray(R.array.imageUrl);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        asyncProgressBar = (ProgressBar) findViewById(R.id.progressBarAsync);
        asyncTaskButton = (Button) findViewById(R.id.buttonAsync);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void downLoadImagesUsingOnlyThread(View view){
        String url = editText.getText().toString();
        Thread ownThread = new Thread(new MyThreadToDownloadImage(url));
        ownThread.start();
    }

    public void downLoadImagesUsingAsyncTask(View view){
        if (editText.getText().toString() !=null && editText.getText().toString().length()>0){
            MyTask myTask = new MyTask();
            myTask.execute(editText.getText().toString()); /*The parameter of execute is string because first param of AsyncTask<> is string*/
        }
    }

    public boolean downloadImageFromUrlUsingThreads(String url)  {
        boolean success = false;
        URL downloadUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        File file = null;
        byte [] buffer = new byte[1024];

        try {
            downloadUrl = new URL(url);
            connection = (HttpURLConnection) downloadUrl.openConnection();
            inputStream = connection.getInputStream();
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                    .getAbsolutePath()+"/"+ Uri.parse(url).getLastPathSegment());
            fileOutputStream = new FileOutputStream(file);
            int read = -1;
            while ((read = inputStream.read(buffer) )!= -1){
               // Log.d("Error Msg", "Msg"+read);
                fileOutputStream.write(buffer,0,read);
            }

            success = true;
          //  loadingSectionb.setVisibility(View.GONE);
        }


        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingSectionb.setVisibility(View.GONE);
                }
            });
            if(connection != null){
                connection.disconnect();
            }
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fileOutputStream != null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return success;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        editText.setText(listOfImage[position]);
    }

    public class MyThreadToDownloadImage implements Runnable{
        private String url;
        MyThreadToDownloadImage(String url){
            this.url = url;
        }

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingSectionb.setVisibility(View.VISIBLE);
                }
            });
            downloadImageFromUrlUsingThreads(listOfImage[0]);
        }
    }
    class MyTask extends AsyncTask<String ,Integer ,Boolean>{
        /*1st :Params , 2nd Progress , 3rd Result*/
        private int contentLength = -1;
        private int counter = 0;
        private int calculateProgress= 0;
        @Override
        protected void onPreExecute() {
            asyncProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            /*First paramter should be same as paramater used in this method *
            Third parameter should be same as return type of do in background method
             */
            boolean success = false;
            URL downloadUrl = null;
            HttpURLConnection connection = null;
            InputStream inputStream = null;
            FileOutputStream fileOutputStream = null;
            File file = null;
            byte [] buffer = new byte[1024];

            try {
                downloadUrl = new URL(params[0]);
                connection = (HttpURLConnection) downloadUrl.openConnection();
                contentLength = connection.getContentLength();
                inputStream = connection.getInputStream();
                file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .getAbsolutePath()+"/"+ Uri.parse(params[0]).getLastPathSegment());
                fileOutputStream = new FileOutputStream(file);
                int read = -1;
                while ((read = inputStream.read(buffer) )!= -1){
                    // Log.d("Error Msg", "Msg"+read);
                    fileOutputStream.write(buffer,0,read);
                    counter = counter + read;
                    publishProgress(counter);

                }

                success = true;
                //  loadingSectionb.setVisibility(View.GONE);
            }


            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if(connection != null){
                    connection.disconnect();
                }
                if (inputStream != null){
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(fileOutputStream != null){
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return success;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            calculateProgress = (int)(((double)values[0]/contentLength)*100);
            asyncProgressBar.setProgress(calculateProgress);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            asyncProgressBar.setVisibility(View.GONE);
        }
    }
}

